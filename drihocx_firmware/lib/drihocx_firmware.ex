defmodule DrihocxFirmware do
  @moduledoc """
  Documentation for DrihocxFirmware.
  """

  @doc """
  Hello world.

  ## Examples

      iex> DrihocxFirmware.hello
      :world

  """
  def hello do
    :world
  end
end
