import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :drihocx_ui, DrihocxUiWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "AQ7bpP2VSwxrJe31lt7uSnN3je39FWW4ngVJQnGlKF1+pjZnmGkNpxTMR1T9TK0I",
  server: false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
