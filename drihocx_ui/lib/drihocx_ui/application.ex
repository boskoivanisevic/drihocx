defmodule DrihocxUi.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      DrihocxUiWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: DrihocxUi.PubSub},
      # Start the Endpoint (http/https)
      DrihocxUiWeb.Endpoint,
      {Mpdex,
       host: Application.get_env(:drihocx_ui, :mpd_host, "127.0.0.1"),
       port: Application.get_env(:drihocx_ui, :mpd_port, 6600)}
      # Start a worker by calling: DrihocxUi.Worker.start_link(arg)
      # {DrihocxUi.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: DrihocxUi.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    DrihocxUiWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
