defmodule DrihocxUiWeb.PlayerLive.Index do
  use DrihocxUiWeb, :live_view

  import DrihocxUiWeb.MultimediaComponents

  @impl true
  def mount(_params, _session, socket) do
    {:ok, queue} = mpd_client().queue()

    socket =
      socket
      |> assign(:player_status, mpd_client().status())
      |> assign(:player_queue, queue)
      |> assign_current_display("queue", "")

    mpd_client().subscribe()

    {:ok, socket}
  end

  @impl true
  def handle_event("show_content", %{"type" => type, "path" => path}, socket) do
    {:noreply, assign_current_display(socket, type, path)}
  end

  @impl true
  def handle_event("select_track", %{"type" => "file", "position" => position}, socket) do
    {:noreply,
     assign(socket, :tracks, select_track(socket.assigns.tracks, String.to_integer(position)))}
  end

  @impl true
  def handle_event("select_track", %{"type" => "file", "path" => path}, socket) do
    tracks = socket.assigns.tracks
    newly_selected_idx = Enum.find_index(tracks, fn track -> path == track.file end)

    {:noreply, assign(socket, :tracks, select_track(tracks, newly_selected_idx))}
  end

  @impl true
  def handle_event("select_track", _, socket), do: {:noreply, socket}

  # --- Player handlers - begin --- #

  @impl true
  def handle_event("previous_track", _, socket) do
    mpd_client().previous()

    {:noreply, socket}
  end

  @impl true
  def handle_event("pause", _, socket) do
    mpd_client().pause()

    {:noreply, socket}
  end

  @impl true
  def handle_event("play", _, socket = %{assigns: %{breadcrumbs: [top | _], tracks: tracks}}) do
    selected = selected_track(tracks)

    case top do
      {"queue", ""} ->
        mpd_client().play(:id, selected.metadata.id)

      _ ->
        mpd_client().clear()
        mpd_client().add_to_queue(selected.file)
        mpd_client().play(:position, 0)
    end

    Process.send_after(self(), :refresh_status, 1000)

    {:noreply, assign_current_display(socket, "queue", "")}
  end

  @impl true
  def handle_event("resume", _, socket) do
    mpd_client().resume()

    {:noreply, socket}
  end

  @impl true
  def handle_event("stop", _, socket) do
    mpd_client().stop()

    {:noreply, socket}
  end

  @impl true
  def handle_event("next_track", _, socket) do
    mpd_client().next()

    {:noreply, socket}
  end

  @impl true
  def handle_event("volume_down", _, socket) do
    if socket.assigns.player_status.volume > 0 do
      mpd_client().volume(socket.assigns.player_status.volume - 1)
    end

    {:noreply, socket}
  end

  @impl true
  def handle_event("volume_up", _, socket) do
    if socket.assigns.player_status.volume < 100 do
      mpd_client().volume(socket.assigns.player_status.volume + 1)
    end

    {:noreply, socket}
  end

  @impl true
  def handle_event("mute", _, socket) do
    if 0 < socket.assigns.player_status.volume,
      do: mpd_client().volume(0),
      else: mpd_client().volume(5)

    {:noreply, socket}
  end

  @impl true
  def handle_event("repeat", _, socket) do
    if 0 == socket.assigns.player_status.repeat,
      do: mpd_client().repeat_on(),
      else: mpd_client().repeat_off()

    {:noreply, socket}
  end

  # --- Player handlers - end --- #

  # --- Player popup menu handlers - begin --- #

  @impl true
  def handle_event("random", _, socket) do
    if 0 == socket.assigns.player_status.random,
      do: mpd_client().random_on(),
      else: mpd_client().random_off()

    {:noreply, socket}
  end

  @impl true
  def handle_event("clear_queue", _, socket) do
    mpd_client().clear()

    {:noreply, assign_current_display(socket, "queue", "")}
  end

  @impl true
  def handle_event(event = "fill_add_url_to_playlist", %{"option" => "0"}, socket) do
    [top | _] = socket.assigns.breadcrumbs

    case top do
      {"playlist", playlist} ->
        # We are displaying content of the playlist and "Add URL to
        # playlist" is selected in the player pop-up menu
        {:noreply,
         socket
         |> assign(:mpd_option, event)
         |> assign(:selected_playlist, playlist)}

      _ ->
        {:noreply,
         socket
         |> assign(:mpd_option, event)}
    end
  end

  # Case when we are displaying list of playlists and "Add URL" option
  # is selected on the pop-up mmenu for one of the displayed
  # playlists.
  @impl true
  def handle_event(event = "fill_add_url_to_playlist", %{"option" => playlist}, socket) do
    {:noreply,
     socket
     |> assign(:mpd_option, event)
     |> assign(:selected_playlist, playlist)}
  end

  @impl true
  def handle_event(event, options, socket)
      when event in [
             "fill_save_queue_to_playlist",
             "fill_crossfade",
             "fill_add_url_to_queue",
             "fill_save_to_playlist"
           ] do
    if "fill_save_to_playlist" == event do
      {:noreply,
       socket
       |> assign(:mpd_option, event)
       |> assign(:selected_playlist, nil)
       |> assign(:path, Map.get(options, "option"))}
    else
      {:noreply,
       socket
       |> assign(:mpd_option, event)}
    end
  end

  @impl true
  def handle_event("fill_rename", %{"option" => playlist}, socket) do
    form = to_form(%{"name" => "", "playlist" => playlist})

    {:noreply,
     socket
     |> assign(:rename_playlist, true)
     |> assign(:form, form)}
  end

  @impl true
  def handle_event("delete", %{"option" => idx}, socket) do
    [current | _] = socket.assigns.breadcrumbs

    case current do
      {"queue", _} -> mpd_client().remove_from_queue(start: idx)
      {"playlist", playlist} -> mpd_client().delete_song_at(playlist, idx)
    end

    socket = assign_current_display(socket, "refresh", "")

    {:noreply, socket}
  end

  @impl true
  def handle_event("single", %{"option" => value}, socket) do
    if "1" == value do
      mpd_client().single_off()
    else
      mpd_client().single_on()
    end

    {:noreply, assign(socket, :player_status, mpd_client().status())}
  end

  @impl true
  def handle_event("shuffle", _, socket) do
    mpd_client().shuffle_queue([])

    {:noreply, socket}
  end

  @impl true
  def handle_event("update_db", _, socket) do
    mpd_client().update_db()

    {:noreply, socket}
  end

  @impl true
  def handle_event("statistics", _, socket) do
    stats = mpd_client().statistics()

    {:noreply, assign(socket, :statistics, stats)}
  end

  @impl true
  def handle_event("reset_connection", _, socket) do
    Process.send(mpd_client(), :reset_connection, [])

    {:noreply, socket}
  end

  @impl true
  def handle_event("close_modal", %{"id" => id}, socket) do
    case id do
      "dlg-mpd-option" ->
        {:noreply, assign(socket, :mpd_option, nil)}

      "dlg-rename-playlist" ->
        {:noreply, assign(socket, :rename_playlist, nil)}

      "dlg-mpd-statistics" ->
        {:noreply, assign(socket, :statistics, nil)}

      _ ->
        {:noreply, socket}
    end
  end

  # --- Player popup menu handlers - end --- #

  # --- Track actions and popup menu handlers - begin --- #

  @impl true
  def handle_event("enqueue_and_play", params, socket) do
    %{"file" => file, "type" => type} = params

    case type do
      "file" ->
        mpd_client().clear()
        mpd_client().add_to_queue(file)

      "playlist" ->
        mpd_client().clear()
        mpd_client().load(file)

      "directory" ->
        fetch_recursive_flat(file)
        |> Enum.each(fn t -> mpd_client().add_to_queue(t.file) end)
    end

    mpd_client().play(:position, 0)

    {:noreply, assign_current_display(socket, "queue", "")}
  end

  @impl true
  def handle_event("enqueue", params, socket) do
    %{"file" => file, "type" => type} = params

    case type do
      "file" ->
        mpd_client().add_to_queue(file)

      "playlist" ->
        mpd_client().load(file)

      "directory" ->
        fetch_recursive_flat(file)
        |> Enum.each(fn t -> mpd_client().add_to_queue(t.file) end)
    end

    {:noreply, socket}
  end

  @impl true
  def handle_event("rename_playlist", %{"playlist" => playlist, "name" => name}, socket) do
    mpd_client().rename(playlist, name)

    {:noreply,
     socket
     |> assign(:rename_playlist, nil)
     |> assign_current_display("refresh", "")}
  end

  @impl true
  def handle_event("clear_list", %{"option" => playlist}, socket) do
    mpd_client().clear_list(playlist)

    {:noreply, socket}
  end

  @impl true
  def handle_event("delete_list", %{"option" => playlist}, socket) do
    mpd_client().delete(playlist)

    {:noreply, assign_current_display(socket, "playlist", "")}
  end

  # --- Track actions and popup menu handlers - end --- #

  # --- Modal dialog action handlers - begin --- #

  @impl true
  def handle_event(
        "save_queue_to_playlist",
        %{
          "create_new" => create_new,
          "new_playlist_name" => new_playlist,
          "playlist" => playlist
        },
        socket
      ) do
    new_playlist = String.trim(new_playlist)

    cond do
      "true" == create_new && 0 < String.length(new_playlist) ->
        mpd_client().save_queue_to_playlist(new_playlist)

      "true" != create_new && 0 < String.length(playlist) ->
        Map.get(socket.assigns, :player_queue, [])
        |> Enum.each(fn track ->
          mpd_client().add_to_list(playlist, track.file)
        end)

      true ->
        nil
    end

    {:noreply, assign(socket, :mpd_option, nil)}
  end

  @impl true
  def handle_event("crossfade", %{"crossfade" => crossfade}, socket) do
    mpd_client().crossfade(String.to_integer(crossfade))

    socket =
      socket
      |> assign(:mpd_option, nil)
      |> assign(:player_status, mpd_client().status())

    {:noreply, socket}
  end

  @impl true
  def handle_event("add_url_to_queue", %{"url" => url}, socket) do
    mpd_client().add_to_queue(url)

    {:noreply, assign(socket, :mpd_option, nil)}
  end

  @impl true
  def handle_event(
        "add_url_to_playlist",
        params = %{"url" => url, "playlist" => playlist},
        socket
      ) do
    [current | _] = socket.assigns.breadcrumbs

    playlist =
      if "true" == Map.get(params, "create_new"),
        do: String.trim(Map.get(params, "new_playlist_name")),
        else: String.trim(playlist)

    if 0 < String.length(playlist), do: mpd_client().add_to_list(playlist, url)

    case current do
      {"playlist", current_playlist} when current_playlist == playlist ->
        {:noreply,
         socket
         |> assign(:mpd_option, nil)
         |> assign(:selected_playlist, nil)
         |> assign_current_display("refresh", "")}

      _ ->
        {:noreply,
         socket
         |> assign(:mpd_option, nil)
         |> assign(:selected_playlist, nil)}
    end
  end

  @impl true
  def handle_event(
        "save_to_playlist",
        %{"path" => path, "new_playlist_name" => new_playlist, "playlist" => playlist},
        socket
      ) do
    playlist_name =
      if 0 < String.length(String.trim(new_playlist)),
        do: String.trim(new_playlist),
        else: playlist

    mpd_client().add_to_list(playlist_name, path)

    {:noreply, assign(socket, :mpd_option, nil)}
  end

  # --- Modal dialog action handlers - end --- #

  @impl true
  def handle_info({:player_status, status}, socket) do
    {:noreply, assign(socket, :player_status, status)}
  end

  @impl true
  def handle_info({:player_queue, queue}, socket) do
    if [{"queue", ""}] == Map.get(socket.assigns, :breadcrumbs) do
      {:noreply,
       socket
       |> assign(:player_queue, queue)
       |> assign(:tracks, queue)}
    else
      {:noreply, assign(socket, :player_queue, queue)}
    end
  end

  @impl true
  def handle_info(:refresh_status, socket) do
    status = Mpdex.status()

    if "play" == status.state, do: Process.send_after(self(), :refresh_status, 1000)

    {:noreply, assign(socket, :player_status, status)}
  end

  defp assign_current_display(socket, what, path) do
    breadcrumbs =
      case what do
        "refresh" ->
          socket.assigns.breadcrumbs

        "up" ->
          Enum.drop(socket.assigns.breadcrumbs, 1)

        _ ->
          if "" == path,
            do: [{what, path}],
            else: [{what, path} | socket.assigns.breadcrumbs]
      end

    {what, path} =
      if "up" == what || "refresh" == what do
        [current | _] = breadcrumbs
        current
      else
        {what, path}
      end

    # Keep selected track
    selected = selected_track(Map.get(socket.assigns, :tracks, []))

    tracks =
      cond do
        "" == path || 1 == length(breadcrumbs) ->
          fetch_tracks(what, path)

        true ->
          [up_track() | fetch_tracks(what, path)]
      end

    socket
    |> assign(:breadcrumbs, breadcrumbs)
    |> assign(:tracks, select_track(tracks, selected))
  end

  defp up_track do
    %{
      file: "",
      metadata: %{
        title: "",
        artist: "",
        duration: nil,
        album: nil
      },
      type: :up
    }
  end

  defp fetch_tracks("queue", _) do
    case mpd_client().queue() do
      {:ok, queue} -> queue
      _ -> []
    end
  end

  defp fetch_tracks("playlist", list) do
    res =
      if "" == list do
        mpd_client().playlists()
      else
        mpd_client().get(list)
      end

    case res do
      {:ok, tracks} -> tracks
      _ -> []
    end
  end

  defp fetch_tracks("directory", directory) do
    res =
      if "" == directory do
        mpd_client().list_db("")
      else
        mpd_client().lsinfo(directory)
      end

    case res do
      {:ok, tracks} -> tracks
      _ -> []
    end
  end

  # Fetches recursively content of directory and returns returns as
  # flat list of songs/streams.
  defp fetch_recursive_flat(path) do
    {:ok, content} = mpd_client().lsinfo(path)

    content
    |> Enum.reduce([], fn track, acc ->
      case track do
        %{type: :file} ->
          [track | acc]

        %{type: :playlist} ->
          [fetch_tracks("playlist", track.file) | acc]

        %{type: :directory} ->
          [fetch_recursive_flat(track.file) | acc]
      end
    end)
    |> List.flatten()
    |> Enum.reverse()
  end

  defp mpd_client(), do: Application.get_env(:drihocx_ui, :mpd_client, Mpdex)

  defp select_track(tracks, newly_selected_idx) when is_integer(newly_selected_idx) do
    currently_selected_idx =
      Enum.find_index(tracks, fn track -> Map.get(track, :selected, false) end)

    cond do
      currently_selected_idx == newly_selected_idx ->
        current_track = Enum.at(tracks, currently_selected_idx) |> Map.delete(:selected)

        List.replace_at(tracks, currently_selected_idx, current_track)

      is_nil(currently_selected_idx) ->
        new_track = Enum.at(tracks, newly_selected_idx) |> Map.put(:selected, true)

        List.replace_at(tracks, newly_selected_idx, new_track)

      true ->
        current_track = Enum.at(tracks, currently_selected_idx) |> Map.delete(:selected)
        new_track = Enum.at(tracks, newly_selected_idx) |> Map.put(:selected, true)

        tracks
        |> List.replace_at(currently_selected_idx, current_track)
        |> List.replace_at(newly_selected_idx, new_track)
    end
  end

  defp select_track(tracks, %{file: file}) do
    track_idx = Enum.find_index(tracks, fn track -> track.file == file end)

    select_track(tracks, track_idx)
  end

  defp select_track(tracks, nil), do: tracks

  defp selected_track(tracks), do: Enum.find(tracks, fn t -> Map.get(t, :selected, false) end)

  defp currently_playing(%{state: state, current_song: %{title: title}}, _)
       when "play" == state or "pause" == state,
       do: title

  defp currently_playing(%{state: state, current_song: %{name: name}}, _)
       when "play" == state or "pause" == state,
       do: name

  defp currently_playing(%{state: state, current_song: %{file: file}}, _)
       when "play" == state or "pause" == state,
       do: Path.basename(file, Path.extname(file))

  defp currently_playing(%{state: state, current_song: %{id: id}}, queue)
       when "play" == state or "pause" == state do
    queue
    |> Enum.find(&(&1.metadata.id == id))
    |> get_in([:metadata, :title])
  end

  defp currently_playing(%{state: state, songid: id}, queue)
       when "play" == state or "pause" == state do
    queue
    |> Enum.find(&(&1.metadata.id == id))
    |> get_in([:metadata, :title])
  end

  defp currently_playing(_, _), do: raw("&nbsp;")

  defp format_statistics_value(key, value) do
    case key do
      :db_playtime ->
        DrihocxUiWeb.MultimediaComponents.display_duration(value)

      :playtime ->
        DrihocxUiWeb.MultimediaComponents.display_duration(value)

      :uptime ->
        DrihocxUiWeb.MultimediaComponents.display_duration(value)

      :db_update ->
        Calendar.strftime(value, "%Y-%m-%d %H:%M")

      _ ->
        value
    end
  end
end
