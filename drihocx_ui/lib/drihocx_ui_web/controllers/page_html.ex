defmodule DrihocxUiWeb.PageHTML do
  use DrihocxUiWeb, :html

  embed_templates "page_html/*"
end
