defmodule DrihocxUiWeb.SvgHTML do
  use DrihocxUiWeb, :html

  embed_templates "svg_html/*"
end
