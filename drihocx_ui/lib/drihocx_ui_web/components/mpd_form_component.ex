defmodule DrihocxUiWeb.MpdFormComponent do
  use DrihocxUiWeb, :live_component

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.simple_form for={@form} id="set-mpd-option-form" phx-submit={@mpd_option}>
        <.input type="hidden" field={@form[:path]} />
        <.input :if={:crossfade == assigns.mpd_option} type="number" field={@form[:crossfade]} />

        <.input
          :if={Enum.member?([:add_url_to_playlist, :add_url_to_queue], assigns.mpd_option)}
          field={@form[:url]}
          label="URL"
        />

        <%= if Enum.member?([:add_url_to_playlist, :save_to_playlist, :save_queue_to_playlist], assigns.mpd_option) do %>
          <.input
            type="select"
            field={@form[:playlist]}
            prompt="Select playlist"
            style={playlist_select_style(assigns)}
            options={@playlists}
            value={@selected_playlist}
            label="Playlist"
          />

          <div :if={is_nil(@selected_playlist)} class="flex flex-row gap-6 items-center h-12">
            <.input
              type="checkbox"
              field={@form[:create_new]}
              phx-click={JS.toggle(to: "#new-playlist-wrapper")}
              label="Create new"
            />

            <div id="new-playlist-wrapper" class="hidden flex-grow">
              <.input id="new-playlist-name" field={@form[:new_playlist_name]} />
            </div>
          </div>
        <% end %>

        <:actions>
          <.button phx-disable-with="Saving...">Save</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def mount(socket) do
    {:ok, socket}
  end

  @impl true
  def update(%{mpd_option: "fill_save_queue_to_playlist"}, socket) do
    form =
      to_form(%{
        "playlist" => "",
        "create_new" => false,
        "new_playlist_name" => "",
        "path" => ""
      })

    {:ok, playlists} = mpd_client().playlists()

    {:ok,
     assign(socket,
       form: form,
       mpd_option: :save_queue_to_playlist,
       path: "",
       selected_playlist: nil,
       playlists: Enum.map(playlists, fn pl -> pl.metadata.title end)
     )}
  end

  @impl true
  def update(%{mpd_option: "fill_crossfade"}, socket) do
    status = mpd_client().status()
    form = to_form(%{"crossfade" => Map.get(status, :xfade, 0)})

    {:ok, assign(socket, form: form, mpd_option: :crossfade)}
  end

  @impl true
  def update(%{mpd_option: "fill_add_url_to_queue"}, socket) do
    form = to_form(%{"url" => ""})

    {:ok, assign(socket, form: form, mpd_option: :add_url_to_queue)}
  end

  @impl true
  def update(assigns = %{mpd_option: "fill_add_url_to_playlist"}, socket) do
    form = to_form(%{"url" => ""})

    playlists =
      case mpd_client().playlists() do
        {:ok, playlists} -> playlists
        _ -> []
      end

    {:ok,
     assign(socket,
       form: form,
       mpd_option: :add_url_to_playlist,
       selected_playlist: Map.get(assigns, :selected_playlist),
       playlists: Enum.map(playlists, fn pl -> pl.metadata.title end)
     )}
  end

  @impl true
  def update(assigns = %{mpd_option: "fill_save_to_playlist", path: path}, socket) do
    form =
      to_form(%{
        "playlist" => "",
        "create_new" => false,
        "new_playlist_name" => "",
        "path" => path
      })

    {:ok, playlists} = mpd_client().playlists()

    {:ok,
     assign(socket,
       form: form,
       mpd_option: :save_to_playlist,
       path: path,
       selected_playlist: Map.get(assigns, :selected_playlist),
       playlists: Enum.map(playlists, fn pl -> pl.metadata.title end)
     )}
  end

  defp mpd_client(), do: Application.get_env(:drihocx_ui, :mpd_client, Mpdex)

  defp playlist_select_style(assigns) do
    case Map.get(assigns, :selected_playlist) do
      nil -> ""
      "" -> ""
      _ -> "pointer-events: none;"
    end
  end
end
