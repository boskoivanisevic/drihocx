defmodule DrihocxUiWeb.MultimediaComponents do
  use Phoenix.Component

  import DrihocxUiWeb.CoreComponents

  alias Phoenix.LiveView.JS

  embed_templates("multimedia/*")

  def side_navigation(assigns) do
    ~H"""
    <div
      id="multimedia-nav"
      class="flex flex-col flex-0 justify-center items-center text-center text-xs w-20 h-full border border-amber-500 border-solid hidden lg:block"
    >
      <%= for collection <- ~w(queue playlist directory) do %>
        <div
          phx-click="show_content"
          phx-value-type={collection}
          phx-value-path=""
          class={nav_class(collection, @breadcrumbs)}
        >
          <.icon
            :if={"queue" == collection}
            name="hero-queue-list-solid"
            class="w-14 h-14 fill-amber-500 cursor-pointer"
          />

          <.icon
            :if={"playlist" == collection}
            name="hero-list-bullet-solid"
            class="w-14 h-14 fill-amber-500 cursor-pointer"
          />

          <.icon
            :if={"directory" == collection}
            name="hero-folder-solid"
            class="w-14 h-14 fill-amber-500 cursor-pointer"
          />

          <p><%= String.capitalize(collection) %></p>
        </div>
      <% end %>
    </div>
    """
  end

  attr(:tracks, :list, required: true)
  attr(:current_display, :list, required: true)
  attr(:player_state, :string, required: true)
  attr(:current_song_id, :string, required: true)

  def tracks_list(assigns) do
    ~H"""
    <div id="tracks-wrapper" class="flex flex-col overflow-y-auto text-[11px] lg:text-[13px] w-full">
      <div
        :for={{track, index} <- Enum.with_index(@tracks)}
        id={"track-#{index}"}
        class={track_class(track)}
      >
        <.track
          track={track}
          index={real_track_index(index, hd(@tracks))}
          player_state={@player_state}
          current_song_id={@current_song_id}
          menu_items={track_menu_items(track, @current_display, real_track_index(index, hd(@tracks)))}
        />
      </div>
    </div>
    """
  end

  def track(
        assigns = %{
          track: track = %{metadata: metadata},
          index: index,
          menu_items: _
        }
      ) do
    assigns =
      assigns
      |> assign(:metadata, metadata)
      |> assign(:type, track.type)
      |> assign(:file, track.file)
      |> assign(:position, Map.get(metadata, :position))
      |> assign(:menu_id, "track-menu-#{index}")

    ~H"""
    <div class="flex flex-col min-h-[33px] lg:flex-row lg:w-full">
      <div
        phx-click={if @type != :file, do: "show_content", else: "select_track"}
        phx-value-path={@file}
        phx-value-type={@type}
        phx-value-position={@position}
        class="lg:w-[60%] cursor-pointer flex items-center"
      >
        <.track_icon
          type={@type}
          player_state={@player_state}
          playing_now={@current_song_id == Map.get(@metadata, :id, "-1")}
        />
        <.title title={@metadata.title} type={@type} />
      </div>

      <div class="text-right my-auto hidden lg:block lg:w-[20%]">
        <%= display_artist(@track, false) %>
      </div>

      <div class="text-right my-auto hidden lg:block lg:w-[20%]"><%= @metadata.album %></div>

      <div class="lg:hidden my-auto">
        <%= display_artist(@track, true) %>
      </div>
    </div>

    <div :if={:up != @type} class="min-w-[70px] text-right">
      <%= display_duration(Map.get(@metadata, :duration)) %>
    </div>

    <div :if={:up != @type} class="hidden lg:flex flex-row justify-end w-[90px]">
      <.svg_icon
        :if={!Map.has_key?(@metadata, :position)}
        phx-click="enqueue_and_play"
        phx-value-file={@file}
        phx-value-type={@type}
        name="queue_music"
        class="h-5 w-5 fill-amber-500 cursor-pointer"
      />
      <.link phx-click="enqueue" phx-value-file={@file} phx-value-type={@type}>
        <.icon
          :if={!Map.has_key?(@metadata, :position)}
          name="hero-queue-list-solid"
          class="fill-amber-500 cursor-pointer h-5 w-5"
        />
      </.link>

      <%= if Enum.any?(@menu_items) do %>
        <.dropdown with_icons={false} id={@menu_id} class="relative" height="5" width="5">
          <:link
            :for={{label, action, value} <- @menu_items}
            href="#"
            value_option={value}
            click={action}
          >
            <%= label %>
          </:link>
        </.dropdown>
      <% else %>
        <div class="h-5 w-5"></div>
      <% end %>
    </div>
    """
  end

  def track_icon(assigns = %{type: :file, player_state: player_state, playing_now: playing_now}) do
    assigns =
      case {player_state, playing_now} do
        {"play", true} -> assign(assigns, :icon, "hero-play-solid")
        {"pause", true} -> assign(assigns, :icon, "hero-pause-solid")
        _ -> assign(assigns, :icon, "hero-musical-note-solid")
      end

    ~H"""
    <.icon name={@icon} class="w-3 h-3 mr-1 fill-amber-500" />
    """
  end

  def track_icon(assigns = %{type: :directory}) do
    ~H"""
    <.icon name="hero-folder-solid" class="w-3 h-3 mr-2 fill-amber-500" />
    """
  end

  def track_icon(assigns = %{type: :playlist}) do
    ~H"""
    <.icon name="hero-list-bullet-solid" class="w-3 h-3 mr-2 fill-amber-500" />
    """
  end

  def track_icon(assigns = %{type: :up}) do
    ~H"""
    <.svg_icon name="folder-up-arrow" class="h-5 w-5 fill-amber-500 cursor-pointer" />
    """
  end

  def title(assigns = %{type: :up}), do: ~H""

  def title(assigns = %{title: title}) do
    assigns = assign(assigns, :title, title)

    ~H"""
    <%= @title %>
    """
  end

  attr(:player_status, :any, required: true)
  attr(:selected_track, :any)

  def play_pause_btn(assigns) do
    base_class = "w-9 h-9 fill-amber-500 cursor-pointer"

    assigns =
      case {assigns.player_status, assigns.selected_track} do
        {%{state: "play"}, _} ->
          assigns
          |> assign(:action, "pause")
          |> assign(:icon_name, "hero-pause-solid")
          |> assign(:class, base_class)

        {%{state: "stop"}, nil} ->
          assigns
          |> assign(:action, "")
          |> assign(:icon_name, "hero-play-solid")
          |> assign(:class, base_class <> " opacity-40")

        {%{state: "stop"}, _} ->
          assigns
          |> assign(:action, "play")
          |> assign(:icon_name, "hero-play-solid")
          |> assign(:class, base_class)

        {%{state: "pause"}, nil} ->
          assigns
          |> assign(:action, "resume")
          |> assign(:icon_name, "hero-play-solid")
          |> assign(:class, base_class)

        {%{state: "pause", songid: song_id}, %{metadata: %{id: id}}} when song_id == id ->
          IO.puts("Nastavak reprodukcije")

          assigns
          |> assign(:action, "resume")
          |> assign(:icon_name, "hero-play-solid")
          |> assign(:class, base_class)

        {%{state: "pause"}, _} ->
          IO.puts("Otkud ovde?")

          assigns
          |> assign(:action, "play")
          |> assign(:icon_name, "hero-play-solid")
          |> assign(:class, base_class)
      end

    ~H"""
    <.link phx-click={@action}>
      <.icon name={@icon_name} class={@class} />
    </.link>
    """
  end

  attr(:selected_track, :any, required: true)
  attr(:player_queue, :any, required: true)
  attr(:player_status, :any, required: true)
  attr(:current_display, :any, required: true)

  slot(:current_title, required: true)
  slot(:current_duration, required: true)
  slot(:current_elapsed, required: true)
  slot(:volume, required: true)

  def player_controls(assigns)

  attr(:name, :string, required: true)
  attr(:rest, :global, doc: "the arbitrary HTML attributes to add to the SVG icon")

  def svg_icon(assigns) do
    ~H"""
    <svg {@rest}>
      <%= Phoenix.Template.render_to_iodata(DrihocxUiWeb.SvgHTML, @name, "html", %{rest: {@rest}})
      |> Phoenix.HTML.raw() %>
    </svg>
    """
  end

  attr(:id, :string, required: true)
  attr(:height, :string, required: true)
  attr(:width, :string, required: true)
  attr(:with_icons, :boolean, required: true)

  attr(:rest, :global)

  slot :link, required: true do
    attr(:navigate, :string)
    attr(:href, :string)
    attr(:click, :string)
    attr(:value_option, :integer)
    attr(:checked, :boolean)
  end

  def dropdown(assigns) do
    ~H"""
    <div id={"#{@id}"} {@rest} phx-hook="Menu">
      <.link phx-click-away={JS.hide(to: "##{@id}-items")}>
        <.icon name="hero-ellipsis-vertical-solid" class={"h-#{@height} w-#{@width}"} />
      </.link>

      <div
        id={"#{@id}-items"}
        class="hidden absolute z-10 left-[-115px] bg-stone-800 border-solid border-2 rounded-md border-amber-500"
      >
        <%= for link <- @link do %>
          <.link
            phx-click={link.click}
            phx-value-option={link.value_option}
            class="flex items-start px-2 py-[0.3rem] text-xs hover:bg-amber-500 hover:text-black"
          >
            <div :if={@with_icons} class="w-3 h-3">
              <.icon :if={Map.get(link, :checked, false)} name="hero-check-solid" class="w-3 h-3" />
            </div>

            <div class="ml-[3px]">
              <%= render_slot(link) %>
            </div>
          </.link>
        <% end %>
      </div>
    </div>
    """
  end

  def display_duration(duration) when is_integer(duration) or is_float(duration) do
    hours = floor(duration / 3600)

    hours_str =
      hours
      |> Integer.to_string()
      |> String.pad_leading(2, "0")

    minutes = floor((duration - hours * 3600) / 60)

    minutes_str =
      minutes
      |> Integer.to_string()
      |> String.pad_leading(2, "0")

    seconds = floor(duration - hours * 3600 - minutes * 60)

    seconds_str =
      seconds
      |> Integer.to_string()
      |> String.pad_leading(2, "0")

    "#{hours_str}:#{minutes_str}:#{seconds_str}"
  end

  def display_duration(_), do: "00:00:00"

  defp display_artist(%{type: :playlist, metadata: %{last_modified: last_modified}}, _mobile) do
    Calendar.strftime(last_modified, "%Y-%m-%d")
  end

  defp display_artist(%{type: :playlist}, _mobile), do: nil

  defp display_artist(%{metadata: metadata}, mobile) do
    if mobile, do: "#{metadata.artist} / #{metadata.album}", else: metadata.artist
  end

  defp icon_class(selected) do
    if selected,
      do: "text-[#E15A0B] cursor-pointer",
      else: "text-amber-500 cursor-pointer"
  end

  defp nav_class(current, breadcrumbs) do
    {display_type, _} = List.last(breadcrumbs)

    if current == display_type,
      do: "flex flex-col items-center border-2 border-solid border-amber-500",
      else: "flex flex-col items-center"
  end

  defp track_class(track) do
    if Map.get(track, :selected) do
      "track flex flex-row gap-3 items-center place-content-between border border-amber-500 h-[80px] lg:h-[33px] px-1 bg-gray-800 font-black"
    else
      "track flex flex-row gap-3 items-center place-content-between border border-amber-500 h-[80px] lg:h-[33px] px-1 hover:font-bold"
    end
  end

  defp progress_bar_style(status) do
    percent =
      (Map.get(status, :elapsed, 0) * 100 / Map.get(status, :duration, 1))
      |> min(100.0)

    "width: #{percent}%;"
  end

  defp volume_bar_style(%{volume: volume}), do: "width: #{volume}%;"

  defp track_menu_items(track, {"queue", ""}, idx) do
    [
      {"Delete", "delete", track.metadata.position}
      | track_menu_items(track, nil, idx)
    ]
  end

  defp track_menu_items(%{file: _, type: :file}, {"playlist", _}, idx) do
    [
      {"Delete", "delete", idx}
    ]
  end

  defp track_menu_items(%{file: file, type: :file}, _what, _idx) do
    [
      {"Save to playlist", "fill_save_to_playlist", file}
    ]
  end

  defp track_menu_items(%{file: file, type: :directory}, _what, _idx) do
    [
      {"Save to playlist", "fill_save_to_playlist", file}
    ]
  end

  defp track_menu_items(%{file: playlist, type: :playlist}, _what, _idx) do
    [
      {"Rename", "fill_rename", playlist},
      {"Add URL", "fill_add_url_to_playlist", playlist},
      {"Clear", "clear_list", playlist},
      {"Delete", "delete_list", playlist}
    ]
  end

  defp track_menu_items(_, _, _), do: []

  defp real_track_index(index, head_track) do
    case head_track do
      %{type: :up} -> index - 1
      _ -> index
    end
  end
end
