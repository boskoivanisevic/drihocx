defmodule DrihocxUiWeb.Layouts do
  use DrihocxUiWeb, :html

  embed_templates "layouts/*"
end
