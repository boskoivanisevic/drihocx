const Menu = {
  mounted() {
    this.el.addEventListener("click", e => {
      let menu = document.querySelector("#" + this.el.id + "-items")

      if ("" === menu.style.display || "none" === menu.style.display) {
        menu.style.display = "block"

        const tracksRect = document.getElementById("tracks-wrapper").getBoundingClientRect()
        const parentRect = menu.parentElement.getBoundingClientRect()
        const boundingRect = menu.getBoundingClientRect()

        // We are setting "translated" data value on the element
        // because on every even displaying without it menu is not
        // properly displayed because transform is set ot 0px.
        if ("true" !== menu.dataset.translated && boundingRect.bottom > tracksRect.bottom) {
          let right = parentRect.left - boundingRect.right
          menu.style.transform = `translate(${right}px, -${boundingRect.height}px) scale(${1 / window.visualViewport.scale})`
          menu.dataset.translated = "true"
        } else if ("true" !== menu.dataset.translated) {
          let right = parentRect.left - boundingRect.right
          menu.style.transform = `translate(${right}px, 0px)`
          menu.dataset.translated = "true"
        }
      } else {
        menu.style.display = "none"
      }
    })
  }
}

export default Menu
