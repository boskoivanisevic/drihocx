defmodule DrihocxUiWeb.MonitoringLiveTest do
  use DrihocxUiWeb.ConnCase

  import Phoenix.LiveViewTest
  import DrihocxUi.MonitoringsFixtures

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  defp create_monitoring(_) do
    monitoring = monitoring_fixture()
    %{monitoring: monitoring}
  end

  describe "Index" do
    setup [:create_monitoring]

    test "lists all monitorings", %{conn: conn} do
      {:ok, _index_live, html} = live(conn, ~p"/monitorings")

      assert html =~ "Listing Monitorings"
    end

    test "saves new monitoring", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/monitorings")

      assert index_live |> element("a", "New Monitoring") |> render_click() =~
               "New Monitoring"

      assert_patch(index_live, ~p"/monitorings/new")

      assert index_live
             |> form("#monitoring-form", monitoring: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#monitoring-form", monitoring: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/monitorings")

      html = render(index_live)
      assert html =~ "Monitoring created successfully"
    end

    test "updates monitoring in listing", %{conn: conn, monitoring: monitoring} do
      {:ok, index_live, _html} = live(conn, ~p"/monitorings")

      assert index_live |> element("#monitorings-#{monitoring.id} a", "Edit") |> render_click() =~
               "Edit Monitoring"

      assert_patch(index_live, ~p"/monitorings/#{monitoring}/edit")

      assert index_live
             |> form("#monitoring-form", monitoring: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#monitoring-form", monitoring: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/monitorings")

      html = render(index_live)
      assert html =~ "Monitoring updated successfully"
    end

    test "deletes monitoring in listing", %{conn: conn, monitoring: monitoring} do
      {:ok, index_live, _html} = live(conn, ~p"/monitorings")

      assert index_live |> element("#monitorings-#{monitoring.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#monitorings-#{monitoring.id}")
    end
  end

  describe "Show" do
    setup [:create_monitoring]

    test "displays monitoring", %{conn: conn, monitoring: monitoring} do
      {:ok, _show_live, html} = live(conn, ~p"/monitorings/#{monitoring}")

      assert html =~ "Show Monitoring"
    end

    test "updates monitoring within modal", %{conn: conn, monitoring: monitoring} do
      {:ok, show_live, _html} = live(conn, ~p"/monitorings/#{monitoring}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Monitoring"

      assert_patch(show_live, ~p"/monitorings/#{monitoring}/show/edit")

      assert show_live
             |> form("#monitoring-form", monitoring: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#monitoring-form", monitoring: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/monitorings/#{monitoring}")

      html = render(show_live)
      assert html =~ "Monitoring updated successfully"
    end
  end
end
