defmodule DrihocxUi.MonitoringsTest do
  use DrihocxUi.DataCase

  alias DrihocxUi.Monitorings

  describe "monitorings" do
    alias DrihocxUi.Monitorings.Monitoring

    import DrihocxUi.MonitoringsFixtures

    @invalid_attrs %{}

    test "list_monitorings/0 returns all monitorings" do
      monitoring = monitoring_fixture()
      assert Monitorings.list_monitorings() == [monitoring]
    end

    test "get_monitoring!/1 returns the monitoring with given id" do
      monitoring = monitoring_fixture()
      assert Monitorings.get_monitoring!(monitoring.id) == monitoring
    end

    test "create_monitoring/1 with valid data creates a monitoring" do
      valid_attrs = %{}

      assert {:ok, %Monitoring{} = monitoring} = Monitorings.create_monitoring(valid_attrs)
    end

    test "create_monitoring/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Monitorings.create_monitoring(@invalid_attrs)
    end

    test "update_monitoring/2 with valid data updates the monitoring" do
      monitoring = monitoring_fixture()
      update_attrs = %{}

      assert {:ok, %Monitoring{} = monitoring} = Monitorings.update_monitoring(monitoring, update_attrs)
    end

    test "update_monitoring/2 with invalid data returns error changeset" do
      monitoring = monitoring_fixture()
      assert {:error, %Ecto.Changeset{}} = Monitorings.update_monitoring(monitoring, @invalid_attrs)
      assert monitoring == Monitorings.get_monitoring!(monitoring.id)
    end

    test "delete_monitoring/1 deletes the monitoring" do
      monitoring = monitoring_fixture()
      assert {:ok, %Monitoring{}} = Monitorings.delete_monitoring(monitoring)
      assert_raise Ecto.NoResultsError, fn -> Monitorings.get_monitoring!(monitoring.id) end
    end

    test "change_monitoring/1 returns a monitoring changeset" do
      monitoring = monitoring_fixture()
      assert %Ecto.Changeset{} = Monitorings.change_monitoring(monitoring)
    end
  end
end
